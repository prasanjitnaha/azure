#!/bin/bash

# Create a resource group.
az group create --name myResourceGroup --location westeurope

# Create a virtual network.
az network vnet create --resource-group myResourceGroup --name myVnet --subnet-name mySubnet

# Create a public IP address.
az network public-ip create --resource-group myResourceGroup --name myPublicIP

# Create a network security group.
az network nsg create --resource-group myResourceGroup --name myNetworkSecurityGroup

# Create a virtual network card and associate with public IP address and NSG.
az network nic create \
  --resource-group myResourceGroup \
  --name myNic \
  --vnet-name myVnet \
  --subnet mySubnet \
  --network-security-group myNetworkSecurityGroup \
  --public-ip-address myPublicIP

# Create a new virtual machine, this creates SSH keys if not present.
az vm create --resource-group myResourceGroup --name myVM --nics myNic --image UbuntuLTS --generate-ssh-keys

# Open port 22 to allow SSh traffic to host.
az vm open-port --port 22 --port 8080 --resource-group myResourceGroup --name myVM

az vm run-command invoke -g myResourceGroup -n myVm --command-id RunShellScript --scripts "sudo apt-get update && sudo apt-get -y install postgresql-13 && sudo apt install postgis postgresql-13-postgis-3"


apt-get update 

apt-get -y install postgresql-13

apt install postgis postgresql-13-postgis-3

service postgresql start

touch dbscript.sql
chmod 777 dbscript.sql
echo "CREATE USER cyclos WITH PASSWORD 'cyclos';
CREATE DATABASE cyclos4 ENCODING 'UTF-8' TEMPLATE template0 OWNER cyclos;
\c cyclos4
create extension cube;
create extension earthdistance;
create extension postgis;
create extension unaccent; \q" >> dbscript.sql

sudo -u postgres psql -f dbscript.sql

cd /tmp
wget http://www-us.apache.org/dist/tomcat/tomcat-8/v8.5.59/bin/apache-tomcat-8.5.59.zip
unzip apache-tomcat-*.zip
sudo mkdir -p /opt/tomcat
sudo mv apache-tomcat-8.5.59 /opt/tomcat/
sudo sh -c 'chmod +x /opt/tomcat/apache-tomcat-8.5.59/bin/*.sh'
sudo ufw allow 8080/tcp
cd /opt/tomcat/apache-tomcat-8.5.59/conf/
echo "<tomcat-users>
   <role rolename="admin-gui"/>
   <role rolename="manager-gui"/>
   <user username="admin" password="admin_password" roles="admin-gui,manager-gui"/>
</tomcat-users>"  >> tomcat-users.xml




cd /tmp
touch cyclos.sh
chmod 777 cyclos.sh
echo "git config --global user.name "Tanuja Adabala"
git config --global user.email "adabalatanuja2@gmail.com"
git clone https://gitlab.com/adabala/gitlab-terraform-aws.git
cd gitlab-terraform-aws
sudo cp -r cyclos /opt/tomcat/apache-tomcat-8.5.59/webapps" >> cyclos.sh

cd /tmp
sh cyclos.sh

cd /opt/tomcat/apache-tomcat-8.5.59/bin
sudo chmod +x startup.sh
sudo chmod +x catalina.sh

echo "IP address"
ifconfig

cd /opt/tomcat/apache-tomcat-8.5.59/bin
sh startup.sh